import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { ImageUploadService } from './services/image-upload/image-upload.service';
import { ImageUploadController } from './controllers/image-upload/image-upload.controller';

@Module({
  imports: [
    MulterModule.register({
      dest: './files',
    }),
  ],
  providers: [ImageUploadService],
  controllers: [ImageUploadController],
})
export class ImageUploadModule {}
