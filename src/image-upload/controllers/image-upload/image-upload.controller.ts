import {
  Controller,
  Get,
  Param,
  Post,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import {
  editedFileName,
  imageFileFilter,
} from '../../../utils/file-uploading.util';
import * as gm from 'gm';
gm.subClass({ imageMagick: true });

@Controller('image-upload')
export class ImageUploadController {
  @Post()
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './files',
        filename: editedFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async uploadedFile(@UploadedFile() file, @Res() res) {
    gm.subClass({ imageMagick: true });
    gm(
      '/media/ash/New Volume1/practice nest/image-compression-nest/image-compression/files/' +
        file.filename,
    )
      .strip()
      .blur(0.3)
      .quality(12)
      .resize(420, 380, '!')
      .noProfile()
      .compress('LZMA')
      .write(
        '/media/ash/New Volume1/practice nest/image-compression-nest/image-compression/files/' +
          file.filename,
        (err) => {
          if (!err) {
            console.log('done');
            res.status(201).json({
              originalname: file.originalname,
              filename: file.filename,
            });
          } else {
            console.log(err);
            res.status(500).json({});
          }
        },
      );
  }

  @Post('multiple')
  @UseInterceptors(
    FilesInterceptor('image', 20, {
      storage: diskStorage({
        destination: './files',
        filename: editedFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async uploadMultipleFiles(@UploadedFile() files) {
    const response = [];
    files.forEach((file) => {
      const fileResponse = {
        originalname: file.originalname,
        filename: file.filename,
      };
      response.push(fileResponse);
    });
    return response;
  }

  @Get(':imgpath')
  showUploadedFile(@Param('imgpath') image, @Res() res) {
    console.log(image);
    return res.sendFile(image, { root: './files' });
  }
}
